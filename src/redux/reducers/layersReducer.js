import { ActionTypes } from "../constants/action-types";
import { useSelector } from "react-redux";

const intialState = {
  maplayers: [],
};

export const getlayersReducer = (state = intialState, { type, payload }) => {
  // console.log(intialState);
  switch (type) {
    case ActionTypes.GET_LAYERS:
      // console.log(state.maplayers);
      return { ...state, maplayers: payload };
    default:
      return state;
  }
};
export const selectedlayerReducer = (state = intialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.TOGGLE_LAYER:
      return state.find(layer => {
        if (layer.id === payload.id) {
          layer.visible = payload.visibility;
        }
        return layer;
      })
    default:
      return state
  };
}
