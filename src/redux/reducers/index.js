import { combineReducers } from "redux";
import { getlayersReducer } from "./layersReducer";
import { selectedlayerReducer } from "./layersReducer";

const allReducers = combineReducers({

  allLayers: getlayersReducer,
  layer: selectedlayerReducer,

});

export default allReducers;