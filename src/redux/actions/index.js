import { ActionTypes } from "../constants/action-types"

export const selectedLayer = (layer) => {
    return {
        type: ActionTypes.SELECTED_LAYER,
        payload: { id: layer.id, visibility: !layer.visible }
    };
};
export const getLayers = (maplayers) => {
    return {
        type: ActionTypes.GET_LAYERS,
        payload: maplayers,
    };
};