const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
    app.use(
        '/site',
        createProxyMiddleware({
            target: 'https://thuysan.hcmgis.vn/bando/site',
            changeOrigin: true,
            pathRewrite: { '/site': '' },
        })
    );
    app.use(
        '/info',
        createProxyMiddleware({
            target: 'http://localhost:9000/thuysan_hcmgis/web/quanly',
            changeOrigin: true,
            pathRewrite: { '/info': '' },
        })
    );
    app.use(
        '/wmslayer',
        createProxyMiddleware({
            target: 'https://thuduc-covid.hcmgis.vn/geoserver/ows?',
            changeOrigin: true,
            pathRewrite: { '/wmslayer': '' },
        })
    );
};