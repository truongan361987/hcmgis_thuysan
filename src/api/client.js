// A tiny wrapper around fetch(), borrowed from
// https://kentcdodds.com/blog/replace-axios-with-a-simple-custom-fetch-wrapper
import axios from "axios";
export async function client(endpoint) {

  const response = await axios
    .get(endpoint)
    .catch((err) => {
      console.log("Err: ", err);
    });
  // console.log(response.data);
  return response.data

}

client.get = function (endpoint) {
  return client(endpoint)
}

client.post = function (endpoint, body, customConfig = {}) {
  return client(endpoint, { ...customConfig, body })
}
