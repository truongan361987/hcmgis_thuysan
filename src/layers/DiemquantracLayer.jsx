import L from 'leaflet';
import React, { useState, useEffect } from 'react';
import { Marker, FeatureGroup, Popup } from 'react-leaflet';
import MarkerClusterGroup from "react-leaflet-markercluster";
import { InfoCircleOutlined } from '@ant-design/icons';
import '../css/Popup.css';
var fetchData = function fetchData(url, options) {
  let request = fetch(url, options);

  return request
    .then(r => r.json())
    .then(data => data);
}

export default function DiemquantracLayer(props) {

  const [data, setData] = useState([]);
  const quantracIcon = L.icon({
    iconUrl: "https://thuysan.hcmgis.vn/resources/images/quantrac.png",
    iconRetinaUrl: "https://thuysan.hcmgis.vn/resources/images/quantrac.png",
    shadowAnchor: [5, 55],
    shadowSize: [5, 55],
    iconSize: [35, 35],
  });
  useEffect(() => {
    if (props.url) {
      const abortController = new AbortController();

      fetchData(props.url, { signal: abortController.signal }).then(data => {
        setData(data);
      });

      // cancel fetch on component unmount
      return () => {
        abortController.abort();
      };
    }

  }, [props.url]);

  var GroupComponent = MarkerClusterGroup;

  console.info(data);
  //console.log(data);
  return (
    <GroupComponent>
      {data.map(f => (
        <Marker
          icon={quantracIcon}
          key={f.id}
          position={[f.st_y, f.st_x]}
        >
          <Popup minWidth={200} closeButton={false}>

            <div className="pop-heading">
              <div className="pop-title">#{f.id} {f.tendiem}</div>
            </div>
            <div className="pop-body">
              <div className="pop-field"><span>Loại mẫu: </span> {f.loaimau} </div>
              <div className="pop-field"><span>Phường/Xã: </span> {f.tenphuong} </div>
              <div className="pop-field"><span>Quận/Huyện: </span> {f.tenquan} </div>

            </div>
            <div className="pop-footer">
              <a href={"https://thuysan.hcmgis.vn/quanly/ptdiemquantrac/view?id=" + f.id} target="_blank" rel="noopener noreferrer">
                Thông tin chi tiết
              </a>
            </div>
          </Popup>
        </Marker>
      ))}
    </GroupComponent>
  );
}