import L from 'leaflet';
import React, { useState, useEffect } from 'react';
import { Marker, FeatureGroup, Popup } from 'react-leaflet';
import MarkerClusterGroup from "react-leaflet-markercluster";
import {
  InfoCircleOutlined
} from '@ant-design/icons';
import '../css/Popup.css';
var fetchData = function fetchData(url, options) {
  let request = fetch(url, options);

  return request
    .then(r => r.json())
    .then(data => data);
}

export default function HonuoiLayer(props) {

  const [data, setData] = useState([]);

  const honuoiIcon = L.icon({
    iconUrl: "https://thuysan.hcmgis.vn/resources/images/shrimp.png",
    iconRetinaUrl: "https://thuysan.hcmgis.vn/resources/images/shrimp.png",
    shadowAnchor: [5, 55],
    shadowSize: [5, 55],
    iconSize: [30, 30],
  });
  useEffect(() => {
    if (props.url) {
      const abortController = new AbortController();

      fetchData(props.url, { signal: abortController.signal }).then(data => {
        setData(data);
      });

      // cancel fetch on component unmount
      return () => {
        abortController.abort();
      };
    }

  }, [props.url]);

  var GroupComponent = MarkerClusterGroup;

  return (
    <GroupComponent>
      {data.map(f => (
        <Marker
          key={f.id}
          icon={honuoiIcon}
          position={[f.st_y, f.st_x]}
        >
          <Popup minWidth={200} closeButton={false}>

            <div className="pop-heading">
              <div className="pop-title">#{f.id} {f.hoten_hn}</div>
            </div>
            <div className="pop-body">
              <div className="pop-field"><span>Loại nuôi </span> {f.loainuoi} </div>
              <div className="pop-field"><span>Phường/Xã: </span> {f.tenphuong} </div>
              <div className="pop-field"><span>Quận/Huyện: </span> {f.tenquan} </div>

            </div>
            <div className="pop-footer">
              <a href={"https://thuysan.hcmgis.vn/quanly/pthonuoi/view?id=" + f.id} target="_blank" rel="noopener noreferrer">
                Thông tin chi tiết
              </a>
            </div>
          </Popup>
        </Marker>
      ))}
    </GroupComponent>
  );
}