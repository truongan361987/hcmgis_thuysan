import '../css/Map.css';
import { useEffect } from 'react';
import { useLeafletContext } from '@react-leaflet/core';
import L from 'leaflet';


export default function WmsLayers(props) {
  //console.log(props.json());

  const context = useLeafletContext()


  useEffect(() => {

    // const options = { 'layers': props.layers, 'transparent': true, 'format': "image/png", 'info_format': "text/html", 'zIndex': 22 };
    const wmslayer = new L.tileLayer.wms("/wmslayer", props.options)
    const container = context.layerContainer || context.map
    container.addLayer(wmslayer)

    return () => {
      container.removeLayer(wmslayer)
    }

  })

  return null
}
