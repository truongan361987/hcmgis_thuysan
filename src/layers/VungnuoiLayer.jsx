
import { useState, useEffect, useRef } from 'react';
import { GeoJSON } from 'react-leaflet';


import '../css/Popup.css';
import axios from 'axios';


export default function VungnuoiLayer(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      const response = await axios.get(props.url);
      setData(response.data)
    };
    getData();

  }, [props.url])
  if (data) {
    const onEachFeature1 = (feature, layer) => {
      if (feature.properties) {

        layer.bindPopup('<div class="pop-heading"><div class="pop-title">' + feature.properties.ma_hn + '</div></div>'
          + '<div class="pop-body"><div class="pop-field"><span>Phường/Xã: </span>' + feature.properties.tenphuong + '</div>'
          + '<div class="pop-field"><span>Quận/Huyện: </span>' + feature.properties.tenquan + '</div> <i class="fa fa-home fa-5x"></i>'
          + '<div class="pop-footer"><a href="https://thuysan.hcmgis.vn/quanly/pgvungnuoi/view?id=' + feature.properties.id + '"'
          + 'target="blank" rel="noopener noreferrer">Thông tin chi tiết</a>  </div></div>'
        );
      }
    }
    return (

      <GeoJSON key={data} style={() => ({
        color: '#4a83ec',
        weight: 0.5,
        fillColor: "rgb(145 185 244)",
        fillOpacity: 0.5,
      })} data={data} onEachFeature={onEachFeature1} />
    )
  }


}


