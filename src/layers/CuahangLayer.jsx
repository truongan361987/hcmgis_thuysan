
import React, { useState, useEffect } from 'react';
import { Marker, Popup } from 'react-leaflet';
import MarkerClusterGroup from "react-leaflet-markercluster";
import L from 'leaflet';
import '../css/Popup.css';
import { InfoCircleOutlined } from '@ant-design/icons';
var fetchData = function fetchData(url, options) {
  let request = fetch(url, options);

  return request
    .then(r => r.json())
    .then(data => data);
}

export default function CuahangLayer(props) {

  const [data, setData] = useState([]);
  const cuahangIcon = L.icon({
    iconUrl: "https://thuysan.hcmgis.vn/resources/images/shop.png",
    iconRetinaUrl: "https://thuysan.hcmgis.vn/resources/images/shop.png",
    shadowAnchor: [5, 55],
    shadowSize: [5, 55],
    iconSize: [35, 35],
  });
  useEffect(() => {
    if (props.url) {
      const abortController = new AbortController();

      fetchData(props.url, { signal: abortController.signal }).then(data => {
        setData(data);
      });

      // cancel fetch on component unmount
      return () => {
        abortController.abort();
      };
    }

  }, [props.url]);

  var GroupComponent = MarkerClusterGroup;


  return (
    <GroupComponent>
      {data.map(f => (
        <Marker
          key={f.id}
          icon={cuahangIcon}
          position={[f.st_y, f.st_x]}
        >
          <Popup minWidth={200} closeButton={false}>

            <div className="pop-heading">
              <div className="pop-title">#{f.id} {f.ten_cs}</div>
            </div>
            <div className="pop-body">
              <div className="pop-field"><span>Loại hình: </span> {f.loaihinh} </div>
              <div className="pop-field"><span>Địa chỉ: </span> {f.diachi} </div>
              <div className="pop-field"><span>Điện thoại: </span> {f.dienthoai} </div>

            </div>
            <div className="pop-footer">
              <a href={"https://thuysan.hcmgis.vn/quanly/ptcuahang/view?id=" + f.id} target="_blank" rel="noopener noreferrer">
                Thông tin chi tiết
              </a>
            </div>
          </Popup>
        </Marker>
      ))}
    </GroupComponent>
  );

}