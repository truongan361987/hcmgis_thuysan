import { useEffect } from "react";
import { useLeafletContext } from "@react-leaflet/core";
import L from "leaflet";
import "../css/Legend.css";
export default function Legend() {
    const context = useLeafletContext()
    const map = context.map
    useEffect(() => {
        if (map) {
            const legend = L.control({ position: "bottomright" });

            legend.onAdd = () => {
                var div = L.DomUtil.create("div", "legend");
                div.innerHTML += "<h4>Chú giải</h4>";
                div.innerHTML += '<i style="background: #477AC2"></i><span>Water</span><br>';
                div.innerHTML += '<i style="background: #448D40"></i><span>Forest</span><br>';
                div.innerHTML += '<i style="background: #E6E696"></i><span>Land</span><br>';
                div.innerHTML += '<i style="background: #E8E6E0"></i><span>Residential</span><br>';
                div.innerHTML += '<i style="background: #FFFFFF"></i><span>Ice</span><br>';
                div.innerHTML += '<i class="icon" style="background-image: url(../images/shrimp.png);background-repeat: no-repeat;"></i><span>Grænse</span><br>';
                return div;
            };

            legend.addTo(map);
        }
    }, [map]);
    return null;
}


