import React from "react";
import { useState } from "react";
import { Button } from 'antd';
import Toc from "./Toc";
import '../css/SidebarPanel.css';
import {
  DesktopOutlined, BarChartOutlined, CloseSquareOutlined

} from '@ant-design/icons';

const SiderPanel = () => {
  const [state, setState] = useState([{
    visible: false,
  }]);

  function onMenuClick() {
    setState({ visible: !state.visible });
  };

  return (
    <div className="sidebar">
      {state.visible &&
        <div className="sidebar-panel">
          <CloseSquareOutlined

            className="sidebar-close-btn"
            type="caret-left"
            onClick={() => onMenuClick()} />
          <Toc paddingRight={20} />
        </div>
      }
      {!state.visible &&
        <Button
          icon={<BarChartOutlined />}
          className="sidebar__open-btn"
          onClick={() => onMenuClick()} />
      }
    </div>
  );

};

export default (SiderPanel);