
import { MapContainer, TileLayer } from 'react-leaflet';
import { useLeafletContext } from '@react-leaflet/core'
import L from 'leaflet';
import { useEffect } from 'react'

export default function CustomControl(props) {
    const context = useLeafletContext()

    const MapInfo = L.Control.extend({
        onAdd: (map) => {
            const panelDiv = L.DomUtil.create("div", "info");

            map.addEventListener("mousemove", (ev) => {
                panelDiv.innerHTML = `<h2><span>Lat: ${ev.latlng.lat.toFixed(
                    4
                )}</span>&nbsp;<span>Lng: ${ev.latlng.lng.toFixed(4)}</span></h2>`;
                console.log(panelDiv.innerHTML);
            });
            return panelDiv;
        },

        onRemove: function (map) {
            // Nothing to do here
        }
    });

    const mapinfo = function (opts) {
        return new MapInfo(opts);
    }

    useEffect(() => {
        const container = context.layerContainer || context.map

        const control = mapinfo({ position: props.position })
        container.addControl(control)

        return () => {
            container.removeControl(control)
        }
    })

    return null
}


