import React from "react";
import '../css/TreeMenu.css';
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { layerUpdated, layersSelectors } from "./map/mapSlice";
import Container from './DropdownContainer';

const TreeMenu = () => {
    const [menu, setMenu] = useState([]);
    const [layer, setLayer] = useState([{
        layers: []
    }]);
    const allLayers = useSelector(layersSelectors.selectAll)
    const dispatch = useDispatch();

    function loadData() {

        let request = fetch("https://thuysan.hcmgis.vn/resources/menu/menu.json");

        request
            .then(r => r.json())
            .then(data => {
                setMenu(data)
            });
    }
    function onChange(currentNode, selectedNodes) {

        //setNode({ id: currentNode.id })
        // console.log(currentNode);
        if (currentNode.id) {
            dispatch(layerUpdated({ id: currentNode.id, visibility: currentNode.checked }));
        }
    };

    useEffect(() => {
        loadData();
        ///Lọc các lớp muốn hiển thị
        let selectedayers = allLayers.filter(l => {
            return (l.toggleable)
        });
        setLayer({ layers: selectedayers });

    }, []);
    // console.log(menu);
    return (

        <Container data={menu} onChange={onChange} texts={{ placeholder: 'Chọn lớp dữ liệu hiển thị' }}
            className="mdl-demo"
        />
    );

};


export default TreeMenu;