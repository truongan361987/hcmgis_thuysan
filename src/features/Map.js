import React, { useEffect } from "react";
import L from 'leaflet';
import {
  MapContainer,
  LayersControl,
  WMSTileLayer,
  TileLayer,
  ZoomControl,

} from "react-leaflet";
import CuahangLayer from "../layers/CuahangLayer";
import VungnuoiLayer from "../layers/VungnuoiLayer";
import HonuoiLayer from "../layers/HonuoiLayer";
import DiemquantracLayer from "../layers/DiemquantracLayer";
import WmsLayers from '../layers/WmsLayers';
import GiongnuoiLayer from "../layers/GiongnuoiLayer";
import PhuongtienLayer from "../layers/PhuongtienLayer";
import '../css/Map.css';
import { useSelector } from "react-redux";
import Basemap from "./Basemaps";
import { layersSelectors } from "./map/mapSlice";
import TreeMenu from "./TreeMenu";
import Fullscreen from 'react-leaflet-fullscreen-plugin';

L.Icon.Default.imagePath = "https://unpkg.com/leaflet@1.5.0/dist/images/";

const Map = () => {
  const layers = useSelector(layersSelectors.selectAll)
  const options = {
    position: 'bottomright', // change the position of the button can be topleft, topright, bottomright or bottomleft, default topleft
    title: 'Fullscreen !', // change the title of the button, default Full Screen
    titleCancel: 'Exit fullscreen ', // change the title of the button when fullscreen is on, default Exit Full Screen
    content: null, // change the content of the button, can be HTML, default null
    forceSeparateButton: true, // force separate button to detach from zoom buttons, default false
    forcePseudoFullscreen: false, // force use of pseudo full screen even if full screen API is available, default false
    fullscreenElement: false, // Dom element to render in full screen, false by default, fallback to map._container
  };
  const layersTypes = {
    'honuoilayer': HonuoiLayer,
    'cuahanglayer': CuahangLayer,
    'giongnuoilayer': GiongnuoiLayer,
    'phuongtienlayer': PhuongtienLayer,
    'vungnuoilayer': VungnuoiLayer,
    'quantraclayer': DiemquantracLayer,
    'wmslayer': WmsLayers,
  }
  return (

    <MapContainer
      zoomControl={false}
      zoom={13}

      center={[10.725246006689355, 106.63906346012972]}
      minZoom={2}
      className="map">


      <LayersControl position="topright">
        <LayersControl.BaseLayer checked name="HCMGIS">
          <WMSTileLayer
            url="http://pcd.hcmgis.vn/geoserver/ows?"
            layers='hcm_map:hcm_map' />
        </LayersControl.BaseLayer>
        <LayersControl.BaseLayer checked name="Vùng nuôi">
          {/* <WMSTileLayer
            url="http://thuysan.hcmgis.vn/geogis/thuysan/ows?"
            layers='thuysan:pg_vungnuoi' /> */}
        </LayersControl.BaseLayer>
        <LayersControl.BaseLayer name="OpenStreetMap">
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
        </LayersControl.BaseLayer>
        <LayersControl.BaseLayer name="Ảnh vệ tinh">
          <TileLayer
            url='http://mt0.google.com/vt/lyrs=s&hl=en&x={x}&y={y}&z={z}'

          />
        </LayersControl.BaseLayer>
      </LayersControl>
      {/* <Legend /> */}
      <ZoomControl position={'topright'} />
      {/* <CustomControl position={'topright'} /> */}
      {layers.map(layer => {
        if (layer.visible) {

          let LayerComp = layersTypes[layer.type];
          return (
            <LayerComp key={layer.id} {...layer} />
          )
        }
      })}
      <TreeMenu />
      <Basemap />
      <Fullscreen {...options} />
    </MapContainer>

  );

};



export default Map;