import { createSlice, createAsyncThunk, createSelector, createEntityAdapter } from '@reduxjs/toolkit'
import { client } from '../../api/client'

// const initialState = {
//   name: 'maplayers',
//   status: 'idle',
//   error: null,
// }

export const fetchLayers = createAsyncThunk('layers/fetchLayers', async () => {
  const response = await client.get('https://thuysan.hcmgis.vn/bando/site/get-maplayers')
  return response
})

const layersAdapter = createEntityAdapter({
  selectId: (layer) => layer.id,
})

const mapSlice = createSlice({
  name: 'layers',
  initialState: layersAdapter.getInitialState({
    status: 'idle',
    error: null
  }),
  reducers: {
    setAllLayers: layersAdapter.setAll,
    // layerUpdated: layersAdapter.updateOne,
    layerUpdated(state, { payload }) {
      //const { id, visibility } = action.payload
      state.entities[payload.id].visible = payload.visibility
      // console.log(state.entities);
      // const existingPost = state.layers.entities.map((layer) => layer.id === id)
      // if (existingPost) {
      //   existingPost.visible = !visibility
      // }
    },
  },
  extraReducers: {
    [fetchLayers.pending]: (state, action) => {
      state.status = 'loading'
    },
    [fetchLayers.fulfilled]: (state, { payload }) => {
      state.status = 'succeeded'
      layersAdapter.setAll(state, payload)
      // Add any fetched posts to the array
      // state.layers = state.layers.concat(action.payload)
    },
    [fetchLayers.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.payload
    },

  },
})

export const { setAllLayers, layerUpdated } = mapSlice.actions

export default mapSlice.reducer

export const layersSelectors = layersAdapter.getSelectors(
  (state) => state.layers
)
export const selectActiveLayers = createSelector([layersSelectors.selectAll], (layers) => layers.filter(l => (l.visible)))
