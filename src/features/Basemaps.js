
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { layerUpdated, layersSelectors } from "./map/mapSlice";
import { Button, Typography, Switch, List } from 'antd';
import '../css/Basemaps.css';
import "../css/Legend.css";
import { InfoCircleOutlined } from '@ant-design/icons';

const Basemap = () => {
  const [state, setState] = useState([{
    basemapsOpen: false,
    activeBm: 'googlemap'
  }]);
  const [layer, setLayer] = useState([{
    data: [],
  }]);
  const layers = useSelector(layersSelectors.selectAll)
  const dispatch = useDispatch();
  function onChange(id) {

    let lyr = layers.filter(l => {
      return (l.id === id)
    })[0];
    // console.log(lyr);
    dispatch(layerUpdated({ id: lyr.id, visibility: lyr.visible }));

  };
  function onBmBtnClick() {
    setState({ basemapsOpen: !state.basemapsOpen });
  }
  useEffect(() => {
    ///Lọc các lớp muốn hiển thị
    let layer = layers.filter(l => {
      return (l.toggleable)
    });

    setLayer({ data: layer });
  }, []);
  //console.info(layer.data);
  return (
    <div className="basemaps-container">
      <Button
        icon={<InfoCircleOutlined />}
        className="basemap-open-btn"
        onClick={onBmBtnClick} />
      {state.basemapsOpen &&

        <div className="legend">

          <h4>CHÚ GIẢI</h4>
          <i className="icon" style={{ backgroundImage: "url(" + "https://thuysan.hcmgis.vn/resources/images/shrimp.png" + ")", backgroundRepeat: 'no-repeat' }}></i><span>Hộ nuôi</span><br />
          <i className="icon" style={{ backgroundImage: "url(" + "https://thuysan.hcmgis.vn/resources/images/shop.png" + ")", backgroundRepeat: 'no-repeat' }}></i><span>Cửa hàng kinh doanh</span><br />
          <i style={{ background: '#448D40' }}></i><span>Vùng nuôi</span><br />
          <i className="icon" style={{ backgroundImage: "url(" + "https://thuysan.hcmgis.vn/resources/images/boat_1.png" + ")", backgroundRepeat: 'no-repeat' }}></i><span>Phương tiện khai thác</span><br />
          <i className="icon" style={{ backgroundImage: "url(" + "https://thuysan.hcmgis.vn/resources/images/supplier.png" + ")", backgroundRepeat: 'no-repeat' }}></i><span>Giống nuôi</span><br />
          <i className="icon" style={{ backgroundImage: "url(" + "https://thuysan.hcmgis.vn/resources/images/quantrac.png" + ")", backgroundRepeat: 'no-repeat' }}></i><span>Điểm quan trắc</span><br />


        </div>

      }
    </div >
  );

};

export default Basemap;