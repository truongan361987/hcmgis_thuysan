import { Popup, useMapEvents, Marker } from 'react-leaflet';
import { useState } from 'react';
import L from 'leaflet'
import { TileLayer } from 'leaflet';
import { selectActiveLayers } from "./map/mapSlice";
import { useSelector } from 'react-redux';
function GetFeatureInfo() {
    const selectedlayers = useSelector(selectActiveLayers)
    const getUrlLayer = (latlng, map) => {
        const _url = ""
        let _map = map,
            point = _map.latLngToContainerPoint(latlng, _map.getZoom()),
            size = _map.getSize(),
            params = {
                request: 'GetFeatureInfo',
                service: 'WMS',
                srs: 'EPSG:4326',
                styles: '',
                transparent: 'true',
                version: '1.1.1',
                format: 'image/png',
                bbox: _map.getBounds().toBBoxString(),
                height: size.y,
                width: size.x,
                layers: 'thuysan:pt_honuoi',
                query_layers: 'thuysan:pt_honuoi',
                info_format: 'application/json',
                feature_count: 10
            };

        params[params.version === '1.3.0' ? 'i' : 'x'] = Math.round(point.x);
        params[params.version === '1.3.0' ? 'j' : 'y'] = Math.round(point.y);


        return _url + L.Util.getParamString(params, _url, true);
    }
    const [position, setPosition] = useState(null)
    const map = useMapEvents({
        click(e) {
            map.eachLayer(layer => {
                if (layer instanceof TileLayer.WMS) {
                    console.log(layer);
                }
            });
            setPosition(e.latlng)
            const url = getUrlLayer(e.latlng, map)
            // console.log(selectedlayers);
        },

    })

    return position === null ? null : (
        <Marker position={position}>
            <Popup>You are here</Popup>
        </Marker>
    )
}
export default GetFeatureInfo