import React from "react";
import '../css/Toc.css';
import { Typography, Switch, List } from 'antd';
import { useRef, useEffect, useLayoutEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

const { Title } = Typography;


const Toc = (props) => {
  const chart = useRef(null);
  function staHonuoi() {
    let x = am4core.create("chartdiv", am4charts.XYChart);
    x.paddingRight = props.paddingRight;
    x.dataSource.url = "https://thuysan.hcmgis.vn/bando/site/tk-honuoi";
    // Create axes
    let categoryAxis = x.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "ma_vn";
    categoryAxis.title.text = "Loại thuỷ sản";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 20;
    let valueAxis = x.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Số lượng";
    // Create series
    var series = x.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "soluong";
    series.dataFields.categoryX = "ma_vn";
    series.name = "Vùng nuôi";
    series.columns.template.tooltipText = "{name}\nLoại thuỷ sản: {categoryX}\nSố vùng: {valueY}";
    //series.tooltipText = "{name}: [bold]{valueY}[/]";
    x.cursor = new am4charts.XYCursor();
    // Add legend
    x.legend = new am4charts.Legend();
    chart.current = x;

  }
  function staCuahang() {
    let x = am4core.create("chartdiv1", am4charts.XYChart);
    x.paddingRight = props.paddingRight;
    x.dataSource.url = "https://thuysan.hcmgis.vn/bando/site/tk-cuahang";
    // Create axes
    let categoryAxis = x.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "loai_ch";
    categoryAxis.title.text = "Loại thuỷ sản";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 20;
    let valueAxis = x.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Số lượng";
    // Create series
    var series = x.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "soluong";
    series.dataFields.categoryX = "loai_ch";
    series.name = "Cửa hàng kinh doanh";
    series.columns.template.tooltipText = "{name}\nLoại thuỷ sản: {categoryX}\nSố cửa hàng: {valueY}";
    //series.columns.template.fill = am4core.color("green"); // fill
    //series.tooltipText = "{name}: [bold]{valueY}[/]";
    x.cursor = new am4charts.XYCursor();
    // Add legend
    x.legend = new am4charts.Legend();
    chart.current = x;

  }
  function staGiongnuoi() {
    let x = am4core.create("chartdiv2", am4charts.XYChart);
    x.paddingRight = props.paddingRight;
    x.dataSource.url = "https://thuysan.hcmgis.vn/bando/site/tk-giongnuoi";
    // Create axes
    let categoryAxis = x.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "loai_giong";
    categoryAxis.title.text = "Loại giống ";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 20;
    let valueAxis = x.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Số lượng";
    // Create series
    var series = x.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "soluong";
    series.dataFields.categoryX = "loai_giong";
    series.name = "Giống thuỷ sản";
    series.columns.template.tooltipText = "{name}\nLoại giống: {categoryX}\nSố cơ sở: {valueY}";
    //series.tooltipText = "{name}: [bold]{valueY}[/]";
    x.cursor = new am4charts.XYCursor();
    // Add legend
    x.legend = new am4charts.Legend();
    chart.current = x;

  }
  function staPhuongtien() {
    let x = am4core.create("chartdiv3", am4charts.XYChart);
    x.paddingRight = props.paddingRight;
    x.dataSource.url = "https://thuysan.hcmgis.vn/bando/site/tk-phuongtien";
    // Create axes
    let categoryAxis = x.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "created_at";
    categoryAxis.title.text = "Nghề khai thác";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 20;
    let valueAxis = x.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Số lượng";
    // Create series
    var series = x.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "soluong";
    series.dataFields.categoryX = "created_at";
    series.name = "Phương tiên khai thác";
    series.columns.template.tooltipText = "{name}\nNghề khai thác: {categoryX}\nSố lượng: {valueY}";
    //series.tooltipText = "{name}: [bold]{valueY}[/]";
    x.cursor = new am4charts.XYCursor();
    // Add legend
    x.legend = new am4charts.Legend();
    chart.current = x;

  }
  useLayoutEffect(() => {
    staHonuoi();
    staCuahang();
    staGiongnuoi();
    staPhuongtien();

    return () => {


    };
  }, [props.paddingRight]);


  return (
    <div className="invisible-scrollbar">
      <Title style={{ textAlign: "center" }} level={4}>Thống kê</Title>
      <div id="chartdiv" style={{ width: "100%", height: "350px" }}></div>
      <div id="chartdiv1" style={{ width: "100%", height: "350px" }}></div>
      <div id="chartdiv2" style={{ width: "100%", height: "350px" }}></div>
      <div id="chartdiv3" style={{ width: "100%", height: "350px" }}></div>
    </div>
  );

};


export default Toc;