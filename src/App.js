import React from 'react';
import { Layout, Menu } from 'antd';
import MapComponent from "./features/Map";
import SiderPanel from "./features/SiderPanel";
import './App.css';


const { Header, Content, Footer, Sider } = Layout;

function App() {

  return (
    <div className="main" >
      <Layout className="main-app" >

        <SiderPanel />
        <Layout className="layout">
          <Header style={{ padding: '0px 17px' }} >
            <img className="logo" src="https://thuysan.hcmgis.vn/resources/images/logo_cctshcm_light.png" alt="logo" width="100%"
            />
            <Menu
              theme="dark"
              mode="horizontal"
            >
              <Menu.Item key="1"><a href={"https://thuysan.hcmgis.vn/quanly/pthonuoi"} target="_blank" rel="noopener noreferrer">
                Quản lý hộ nuôi
              </a></Menu.Item>
              <Menu.Item key="2"><a href={"https://thuysan.hcmgis.vn/quanly/pgvungnuoi"} target="_blank" rel="noopener noreferrer">
                Quản lý vùng nuôi
              </a></Menu.Item>
              <Menu.Item key="3"><a href={"https://thuysan.hcmgis.vn/quanly/ptcuahang"} target="_blank" rel="noopener noreferrer">
                Quản lý cửa hàng
              </a></Menu.Item>
              <Menu.Item key="4"><a href={"https://thuysan.hcmgis.vn/quanly/ptphuongtienkhaithac"} target="_blank" rel="noopener noreferrer">
                Quản lý Phương tiện khai thác
              </a></Menu.Item>
              <Menu.Item key="5"><a href={"https://thuysan.hcmgis.vn/quanly/ptgiongthuysan"} target="_blank" rel="noopener noreferrer">
                Quản lý Giống thuỷ sản
              </a></Menu.Item>
              <Menu.Item key="6"><a href={"https://thuysan.hcmgis.vn/quanly/ptquantrac"} target="_blank" rel="noopener noreferrer">
                Quản lý Điểm quan trắc
              </a></Menu.Item>

            </Menu>
          </Header>
          <Content className="main-map">
            <MapComponent />
          </Content>
          <Footer style={{ textAlign: 'center' }}>TRUNG TÂM ỨNG DỤNG HỆ THỐNG THÔNG TIN ĐỊA LÝ ©2021 HCMGIS</Footer>
        </Layout>
      </Layout>
    </div >
  );
}

export default App;
